from pathlib import Path
from typing import Any, Dict, List, Union

import gemmi
import haiku as hk
import jax
import numpy as np


def load_model(
    model_name="model_1",
    heads_to_remove=(),
    device=None,
) -> "PatchedRunModel":  # type: ignore  # noqa
    from alphafold.model import config, data
    from alphafold.model.model import RunModel, modules, modules_multimer

    from elaspic2.plugins.alphafold import AlphaFold

    class PatchedRunModel(RunModel):
        def __init__(self, config, params=None):
            self.config = config
            self.params = params
            self.multimer_mode = config.model.global_config.multimer_mode

            if self.multimer_mode:

                def _forward_fn(batch):
                    alphafold_model = modules_multimer.AlphaFold(self.config.model)
                    return alphafold_model(batch, is_training=False)

            else:

                def _forward_fn(batch):
                    alphafold_model = modules.AlphaFold(self.config.model)
                    return alphafold_model(
                        batch,
                        is_training=False,
                        compute_loss=False,  # AS
                        ensemble_representations=True,
                        return_representations=True,
                    )

            self.apply = jax.jit(hk.transform(_forward_fn).apply)
            self.init = jax.jit(hk.transform(_forward_fn).init)

    if device not in ["cpu", "gpu", "tpu"]:
        raise ValueError(f"Unsupported device type: {device!r}.")
    jax.config.update("jax_platform_name", device)

    data_dir = AlphaFold._get_data_dir()

    model_params = data.get_model_haiku_params(
        model_name=model_name + "_ptm", data_dir=str(data_dir)
    )

    cfg = config.model_config(model_name + "_ptm")
    cfg.data.common.use_templates = True
    cfg.model.global_config.multimer_mode = False
    cfg.data.eval.num_ensemble = 1  # AS [1, 8]
    cfg.data.common.num_recycle = 0
    cfg.model.num_recycle = 0
    cfg.model.recycle_tol = 0  # [0,0.1,0.5,1]

    for head in heads_to_remove:
        del cfg.model.heads[head]

    model = PatchedRunModel(cfg, model_params)

    return model


def combine_alignments(sequences: List[str], alignments: List[List[str]]) -> List[str]:
    """Combine alignments for multiple chains into a signle alignment."""
    assert len(sequences) == len(alignments)

    combined_alignment = [
        ">101\n",
        "".join(sequences) + "\n",
    ]
    for i in range(len(alignments)):
        num_before = sum((len(s) for s in sequences[:i]))
        num_after = sum((len(s) for s in sequences[i + 1 :]))
        for line in alignments[i][2:]:
            if not line.strip():
                continue
            if not line.startswith(">"):
                line = "-" * num_before + line.strip() + "-" * num_after + "\n"
            combined_alignment.append(line)
    return combined_alignment


def generate_mapping(sequence, structure):
    mapping = {}
    residue_idx = 0
    residues = list(structure[0]["A"])
    assert len(sequence.replace("X", "")) == len(residues)
    for i, aa in enumerate(sequence):
        if aa == "X":
            continue
        assert gemmi.one_letter_code([residues[residue_idx].name]) == aa
        mapping[i] = residue_idx
        residue_idx += 1
    return mapping


def generate_missing_mask(sequence):
    return np.array([aa == "X" for aa in sequence])


def strip_masked_residues(sequence, structure):
    assert len(sequence) == len(list(structure[0]["A"]))
    residues = []
    for aa, residue in zip(sequence, structure[0]["A"]):
        if aa == "X":
            continue
        assert gemmi.one_letter_code([residue.name]) == aa
        residues.append(residue)
    assert len(sequence.replace("X", "")) == len(list(residues))
    return structure_from_residues(residues)


def write_alphafold_template(structure: gemmi.Structure, output_file: str) -> None:
    """Write a structure in a format that can be parsed by AlphaFold."""
    assert len(list(structure)) == 1
    assert len(list(structure[0])) == 1
    residues = list(structure[0]["A"])

    groups = gemmi.MmcifOutputGroups(True)
    groups.group_pdb = True
    block = structure.make_mmcif_block(groups)

    block.set_pair("_pdbx_audit_revision_history.revision_date", "2100-01-01")

    col = block.find("_chem_comp.", ["id", "type"]).find_column("type")
    for i in range(len(col)):
        col[i] = "'L-peptide linking'"

    loop = block.init_loop("_entity_poly_seq.", ["entity_id", "num", "mon_id", "hetero"])
    for i, residue in enumerate(residues):
        loop.add_row(["1", f"{i + 1}", residue.name, "n"])

    table = block.find("_struct_asym.", ["id", "entity_id"])
    table[0][1] = "1"

    block.write_file(output_file)


def create_feature_dict(
    *,
    sequence: str,
    msa: list[str],
    structure_file: Union[str, List[str]],
    structure_mapping: Union[dict[int, int], List[dict[int, int]]],
    random_seed: float,
) -> dict[str, object]:
    from alphafold.data import pipeline

    from elaspic2.plugins.alphafold import templates

    msa_obj = pipeline.parsers.parse_a3m("".join(msa))
    feature_dict = pipeline.make_sequence_features(
        sequence=sequence, description="none", num_res=len(sequence)
    ) | pipeline.make_msa_features((msa_obj,))

    if structure_file is None:
        feature_dict |= templates.make_mock_template_features(sequence)
    elif isinstance(structure_file, (str, Path)):
        feature_dict |= templates.extract_template_features(
            sequence, structure_file, structure_mapping
        )
    else:
        template_feature_dicts = [
            templates.extract_template_features(sequence, structure_file[i], structure_mapping[i])
            for i in range(len(structure_file))
        ]
        feature_dict |= {
            k: np.vstack([fd[k] for fd in template_feature_dicts])
            for k in template_feature_dicts[0]
        }

    return feature_dict


def apply_multimer_patch(
    feature_dict: Dict[str, Any], sequence_lengths: List[int], offset=300
) -> None:
    seq_length = 0
    for length in sequence_lengths:
        seq_length += length
        feature_dict["residue_index"][seq_length:] += offset


def mask_x_residues(processed_feature_dict):
    mask = (processed_feature_dict["aatype"] == 20).squeeze()
    processed_feature_dict["msa_feat"][:, 0, mask, :23] = 0.0
    processed_feature_dict["msa_feat"][:, 0, mask, 22] = 1.0


def collect_predictions_for_x(sequence, sequence_ref, predictions):
    from alphafold.common import residue_constants

    restype_order_with_x = residue_constants.restype_order_with_x.copy()
    restype_order_with_x_rev = {v: k for k, v in restype_order_with_x.items()}

    candidates = []
    for i, (aa, aa_ref) in enumerate(zip(sequence, sequence_ref)):
        if aa != "X":
            continue

        logits_all = predictions["masked_msa"]["logits"][0, i]
        logits = logits_all[:20]
        # probas = jax.nn.softmax(logits)
        logprobas = jax.nn.log_softmax(logits)

        best_aa_idx = jax.numpy.argmax(logits).item()
        best_aa = restype_order_with_x_rev[best_aa_idx]

        x_aa_idx = restype_order_with_x[aa]
        ref_aa_idx = restype_order_with_x[aa_ref]

        candidates.append(
            {
                "idx": i,
                "aa": aa_ref,
                "aa_best": best_aa,
                "logproba": logprobas[ref_aa_idx].item(),
                "logproba_best": logprobas[best_aa_idx].item(),
                "logproba_x": logprobas[x_aa_idx].item(),
            }
        )
    return candidates


def structure_from_residues(residues, seqnum_mapping=None):
    chain = gemmi.Chain("A")
    for residue_idx, residue in enumerate(residues):
        residue = residue.clone()
        residue.subchain = "A"
        if seqnum_mapping is not None:
            seqnum = seqnum_mapping[residue_idx]
        else:
            seqnum = residue_idx + 1
        residue.seqid.num = seqnum
        residue.label_seq = seqnum
        chain.add_residue(residue)

    model = gemmi.Model("1")
    model.add_chain(chain)

    structure = gemmi.Structure()
    structure.add_model(model)
    structure.add_entity_types()
    structure.assign_label_seq_id()
    return structure


def show_structures(polimer_1, polimer_2) -> None:
    import py3Dmol

    view = py3Dmol.view(width=600, height=600)

    view.addModel(structure_from_residues(polimer_1).make_minimal_pdb(), "pdb")
    view.addModel(structure_from_residues(polimer_2).make_minimal_pdb(), "pdb")

    view.getModel(0).setStyle({"cartoon": {"color": "grey"}})
    view.getModel(1).setStyle({"cartoon": {"color": "blue"}})

    view.zoomTo()
    return view
